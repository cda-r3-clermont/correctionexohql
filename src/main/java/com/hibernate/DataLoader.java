package com.hibernate;

import com.hibernate.model.Category;
import com.hibernate.model.Commande;
import com.hibernate.model.Produit;
import com.hibernate.model.Utilisateur;
import com.hibernate.utils.HibernateUtil;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;

import javax.persistence.criteria.*;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.List;

public class DataLoader {

    public static void main(String[] args) {
        SessionFactory sf = new HibernateUtil().buildSessionFactory();
        Session session = sf.getCurrentSession();
        Transaction tx = session.beginTransaction();

        session.createQuery("DELETE Commande ").executeUpdate();
        session.createQuery("DELETE Produit ").executeUpdate();
        session.createQuery("DELETE Category ").executeUpdate();
        session.createQuery("DELETE Utilisateur").executeUpdate();

        // Création des catégories
        Category category1 = new Category("Salade");
        Category category2 = new Category("Burger");
        Category category3 = new Category("Wrap");

        session.persist(category1);
        session.persist(category2);
        session.persist(category3);


        // Création des produits
        Produit caesarBowl = new Produit("Super salade au poulet",
                "https://burgerkingfrance.twic.pics/img/menu/53dbad75-e564-4cf2-9188-8499015b55c6_?twic=v1/contain=450x450",
                category1,
                8.30F);
        Produit energyChicken = new Produit(
                "Super salade au poulet et fromage",
                "https://burgerkingfrance.twic.pics/img/menu/9def1ddf-66f8-4933-a917-2856a3620708_?twic=v1/contain=450x450",
                category1,
                9.50F);

        Produit onionBbqLover = new Produit(
                "Burger aux oignons",
                    "https://burgerkingfrance.twic.pics/img/products/c/e5/ce59c902-319f-46a8-80ca-f141044c0242_?twic=v1/contain=1100x1100",
                            category2,
                13.40F
        );

        Produit whooper = new Produit(
                "Whooper",
                "https://burgerkingfrance.twic.pics/img/products/4/56/4565c2c0-baec-48d9-8493-db196b9b3250_?twic=v1/contain=700x700",
                    category2,
                6.60F
                );

        Produit croustyChevre = new Produit(
                "Wrap crousty chèvre",
                "https://burgerkingfrance.twic.pics/img/products/a/09/a090be42-4ac4-4d16-8e75-c64ca39218cd_?twic=v1/contain=450x450",
                        category3,
                7.70F
        );

        Produit croustyBacon = new Produit(
                "Wrap crousty bacon",
                "https://burgerkingfrance.twic.pics/img/products/a/09/a090be42-4ac4-4d16-8e75-c64ca39218cd_?twic=v1/contain=450x450",
                category3,
                7.30F
        );

        session.persist(caesarBowl);
        session.persist(energyChicken);
        session.persist(onionBbqLover);
        session.persist(whooper);
        session.persist(croustyChevre);
        session.persist(croustyBacon);

        // Enregistrement des utilisateurs

        Utilisateur adelorme =
                new Utilisateur("Aurélien",
                        new GregorianCalendar(1993, Calendar.MARCH, 30),
                        "aureliendelorme1@gmail.com");

        Utilisateur johan =
                new Utilisateur("Johan",
                        new GregorianCalendar(1988, Calendar.JUNE, 23),
                        "Johan@hb.com");

        Utilisateur mylene =
                new Utilisateur("mylene",
                        new GregorianCalendar(1990, Calendar.JUNE, 23),
                        "mylene@hb.com");

        Utilisateur junior = new Utilisateur("Junior",
                new GregorianCalendar(2010, Calendar.JANUARY,1), "junior@patpatrouille.com");


        session.persist(adelorme);
        session.persist(johan);
        session.persist(mylene);
        session.persist(mylene);
        session.persist(junior);

        // Création des commandes
        Commande adelormeSalade = new Commande();
        adelormeSalade.setProduit(caesarBowl);
        adelormeSalade.setUtilisateur(adelorme);

        Commande adelormeWhooper = new Commande();
        adelormeWhooper.setUtilisateur(adelorme);
        adelormeWhooper.setProduit(whooper);

        Commande johanCroustyChevre = new Commande();
        johanCroustyChevre.setProduit(croustyChevre);
        johanCroustyChevre.setUtilisateur(johan);

        Commande johanCroustyBacon = new Commande();
        johanCroustyBacon.setProduit(croustyBacon);
        johanCroustyBacon.setUtilisateur(johan);

        Commande myleneOnion = new Commande();
        myleneOnion.setUtilisateur(mylene);
        myleneOnion.setProduit(onionBbqLover);

        Commande juniorCommandeWhooper = new Commande();
                juniorCommandeWhooper.setUtilisateur(junior);
                juniorCommandeWhooper.setProduit(whooper);
                session.persist(juniorCommandeWhooper);

        Commande juniorCommandeWrap = new Commande();
        juniorCommandeWrap.setUtilisateur(junior);
        juniorCommandeWrap.setProduit(croustyChevre);

        session.persist(juniorCommandeWrap);

        session.persist(adelormeSalade);
        session.persist(adelormeWhooper);
        session.persist(johanCroustyChevre);
        session.persist(johanCroustyBacon);
        session.persist(myleneOnion);





        tx.commit();
        session.close();
        sf.close();

    }
}
