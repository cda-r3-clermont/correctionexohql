package com.hibernate.model;

import javax.persistence.*;
import java.util.Calendar;

@Entity
@Table(name = "utilisateur")
public class Utilisateur {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;
    @Basic
    private String prenom;

    @Temporal(TemporalType.DATE)
    private Calendar dateNaissance;

    @Basic
    private String email;

    public Utilisateur(String prenom, Calendar dateNaissance, String email) {
        this.prenom = prenom;
        this.dateNaissance = dateNaissance;
        this.email = email;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getPrenom() {
        return prenom;
    }

    public void setPrenom(String prenom) {
        this.prenom = prenom;
    }

    public Calendar getDateNaissance() {
        return dateNaissance;
    }

    public void setDateNaissance(Calendar dateNaissance) {
        this.dateNaissance = dateNaissance;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }
}
