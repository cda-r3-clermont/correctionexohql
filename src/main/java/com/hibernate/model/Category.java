package com.hibernate.model;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

@Entity
@Table(name = "category")
public class Category {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;

    @Basic
    private String libelle;

    @OneToMany(mappedBy = "category")
    private List<Produit> produits;

    public Category() {
    }

    public Category(String libelle) {
        this.libelle = libelle;
    }

    public void Category(){
        this.produits =  new ArrayList<Produit>();
    }

    public List<Produit> getProduits(){
        return  this.produits;
    }

    public void addProduit(Produit produit){
        this.produits.add(produit);
    }

    public void removeProduit(Produit produit){
        this.produits.remove(produit);
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getLibelle() {
        return libelle;
    }

    public void setLibelle(String libelle) {
        this.libelle = libelle;
    }
}
