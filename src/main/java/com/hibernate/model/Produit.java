package com.hibernate.model;

import org.hibernate.annotations.Type;

import javax.persistence.*;

@Entity
@Table(name = "produit")
public class Produit {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;

    @Column
    @Type(type = "text")
    private String description;

    @Basic
    private String image;

    @ManyToOne
    @JoinColumn(name = "category_id")
    private Category category;

    @Basic
    private double prix;

    public Produit() {
    }

    public Produit(String description, String image, Category category, double prix) {
        this.description = description;
        this.image = image;
        this.category = category;
        this.prix = prix;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public Category getCategory() {
        return category;
    }

    public void setCategory(Category category) {
        this.category = category;
    }

    public double getPrix() {
        return prix;
    }

    public void setPrix(double prix) {
        this.prix = prix;
    }
}
