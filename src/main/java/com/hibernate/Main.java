package com.hibernate;

import com.hibernate.model.Produit;
import com.hibernate.utils.HibernateUtil;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;

import javax.persistence.criteria.*;
import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.List;
import java.util.Objects;

public class Main {

        public static void main(String[] args) {
            SessionFactory sf = new HibernateUtil().buildSessionFactory();
            Session session = sf.getCurrentSession();
            Transaction tx = session.beginTransaction();

            // Requête 1 !!
            Long nbUtilisateur =
                    session.createQuery("SELECT COUNT(u) FROM Utilisateur  u", Long.class)
                            .getSingleResult();

            System.out.println("Il y a "+nbUtilisateur+" qui sont entrain de commander !");

            // Requête 2
            List<Long> nbCommandes =
                    session.createQuery("SELECT COUNT(u) FROM Commande c " +
                            "INNER JOIN c.utilisateur u " +
                            "GROUP BY u.id", Long.class ).getResultList();

            System.out.println("Il y a actuellement "+nbCommandes.size()+"commandes en cours");

            // Requête 3
            List<Object[]> moyenneParCategorie =
                    (List<Object[]>) session.createQuery("SELECT c.libelle, AVG(p.prix) FROM Produit p " +
                            "INNER JOIN p.category c GROUP BY c.id").getResultList();

            for (Object[] categMoyenne: moyenneParCategorie){
                System.out.println("Catégorie : " + categMoyenne[0]);
                System.out.println("Moyenne des prix : " + categMoyenne[1]);
                System.out.println("___");
            }

            // Requête 4
            String baconResearch = "%bacon%";

            List<Produit> produits = session.createQuery(
                    "FROM Produit p WHERE p.description LIKE (:param)", Produit.class)
                    .setParameter("param", baconResearch).getResultList();

            for (Produit produit: produits){
                System.out.println(produit.getDescription());
            }

            // Requête 5

            GregorianCalendar gc = new GregorianCalendar();
            gc.add(Calendar.YEAR, -18);


            Double moyenneMineur = session.createQuery(
                    "SELECT AVG(p.prix) FROM Commande c " +
                            "LEFT JOIN c.utilisateur u " +
                            "LEFT JOIN c.produit p WHERE u.dateNaissance > :dateMineur"
                        , Double.class
            ).setParameter("dateMineur", gc).getSingleResult();

            System.out.println("Moyenne des commandes d'un mineur : " + moyenneMineur);


            // Requête 6 :

            Double moyenneMajeur = session.createQuery(
                    "SELECT AVG(p.prix) FROM Commande c " +
                            "LEFT JOIN c.utilisateur u " +
                            "LEFT JOIN c.produit p WHERE u.dateNaissance <= :dateMineur"
                    , Double.class
            ).setParameter("dateMineur", gc).getSingleResult();

            System.out.println("Moyenne des commandes d'un majeur : " + moyenneMajeur);


            // Requête 7

            Double moyen = session.createQuery(
                    "SELECT AVG(p.prix) FROM Commande c " +
                            "LEFT JOIN c.utilisateur u " +
                            "LEFT JOIN c.produit p"
                    , Double.class
            ).getSingleResult();

            System.out.println("Moyenne des commandes : " + moyen);

            tx.commit();
            session.close();
            sf.close();

        }
}
